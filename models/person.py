import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, relationship, backref
import os

basedir = os.path.abspath(os.path.dirname(__file__))
connection_string = "sqlite:///"+os.path.join(basedir,'db_person.sqlite')

engine = sa.create_engine(connection_string)
session = scoped_session(sessionmaker(bind=engine))
Base = declarative_base()

class Person(Base):
    __tablename__ = "persons"
    id = sa.Column(sa.Integer,primary_key=True)
    checked = sa.Column(sa.Boolean)
    name = sa.Column(sa.String(100),nullable= False)
    type = sa.Column(sa.String(100))
    age = sa.Column(sa.Integer)
    description = sa.Column(sa.String(300))
    date = sa.Column(sa.Date)

    def __init__(self,checked,name,type,age,description,date):
        self.checked = checked
        self.name = name
        self.type = type
        self.age = age
        self.description = description
        self.date = date

Base.metadata.create_all(engine)