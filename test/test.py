
from turtle import update
import unittest
import requests
class PersonTest(unittest.TestCase):

    URL = " http://127.0.0.1:5000/person"
    
    data = {  
        "age": 26,
        "checked": 'true',
        "description": "this Student is in the company",
        "name": "Jay Suresh Prajapati",
        "type": "Employer"
    }
    
    update_data = {

        "name": "Shailesh Patel",
        "type": "Master Student"

    }
    #check for response 200
    def test_get_all_person_status(self):
        response = requests.get(self.URL)
        self.assertEqual(response.status_code,200)
        print("Test Pass Successfully")

    #check for response  json length
    def test_get_all_person_content_length(self):
        response = requests.get(self.URL)
        self.assertEqual(len(response.json()),3)
        print("Test get all person response length Pass Successfully")

    #check for get specific person response
    def test_get_person_status(self):
        response = requests.get(self.URL+'/2')
        self.assertEqual(response.status_code,200)
        print("Test get specific person status code Pass Successfully")
        
      #check for get specific person response
    def test_get_person_content_length(self):
        response = requests.get(self.URL+'/2')
        self.assertEqual(len(response.json()),7)
        print("Test get specific person response length  Pass Successfully")
        
    #check for delete specific person response
    def test_delete_person_status(self):
        response = requests.delete(self.URL+'/2')
        self.assertEqual(response.status_code,200)
        print("Test person delete Pass Successfully")
        
    # check for delete specific person response 
    def test_delete_person_content_length(self):
        response = requests.delete(self.URL+'/2')
        self.assertEqual(len(response.json()),1)
        print("Test get specific person response length  Pass Successfully")
        
    # check for update person request
    def test_update_person_status(self):
        response = requests.put(self.URL+'/2',json = self.update_data)
        self.assertEqual(response.status_code,200)
        print("Test update response status code Pass Successfully")
        
   #check for update person request
    def test_update_person_content(self):
        response = requests.put(self.URL+'/2',json= self.update_data)
        self.assertEqual(response.json()['name'],self.update_data['name'])
        self.assertEqual(response.json()['type'],self.update_data['type'])
        print("Test Update response Pass Successfully")
        
     # check for create person request
    def test_create_person_status(self):
        response = requests.post(self.URL,json = self.data)
        self.assertEqual(response.status_code,200)
        print("Test create response status code Pass Successfully")
        
   #check for create person request
    def test_create_person_length(self):
        response = requests.post(self.URL,json= self.data)
        self.assertEqual(len(response.json()),1)
        print("Test create response length Pass Successfully")
    
if __name__ == "__main__":
    tester = PersonTest()    
    tester.test_get_all_person_status()
    tester.test_get_all_person_content_length()
    tester.test_get_person_content_length()
    tester.test_get_person_status()
    tester.test_delete_person_status()
    tester.test_delete_person_content_length()
    tester.test_create_person_status()
    tester.test_create_person_length()
    tester.test_update_person_content()
    tester.test_update_person_status()
    