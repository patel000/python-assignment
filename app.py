import connexion
from connexion import jsonifier,request
import datetime
from models.person import Person,session
from schemas.person_schema import person_schema,persons_schema


#Create or Update Person
def add_person():
    checked = request.json['checked']
    name = request.json['name']
    type = request.json['type']
    age =  request.json['age']
    description =  request.json['description']
   
    new_person = Person(checked,name,type,age,description, datetime.datetime.now())

    session.add(new_person)
    session.commit()

    return person_schema.dump(new_person)

#Get All Persons
def get_persons():
    all_persons = session.query(Person).all()
    result = persons_schema.dump(all_persons)
    return result

# Get Singel Person
def get_person(person_id ):
    person = session.query(Person).get(person_id )
    return person_schema.dump(person)

# Update Single Person
def update_person(person_id):

    person = session.query(Person).get(person_id)
    checked = request.json['checked']
    name = request.json['name']
    type = request.json['type']
    age =  request.json['age']
    description =  request.json['description']

    person.checked = checked
    person.name = name
    person.type = type
    person.age = age
    person.description = description
    person.date = datetime.datetime.now()
    session.commit()

    return person_schema.dump(person)

# delete Singel Person

def delete_person(person_id ):
    person = session.query(Person).get(person_id )
    session.delete(person)
    session.commit()
    return person_schema.dump(person)

app = connexion.App(__name__,specification_dir='static/') 
app.add_api('swagger.yaml') 
application = app.app 
 
if __name__ == '__main__': 
    app.run(port=8080)