from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from models.person import Person

class PersonSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Person
        include_relationships = True
        load_instance = True
        
#Init Schema
person_schema = PersonSchema()
persons_schema = PersonSchema(many=True)


